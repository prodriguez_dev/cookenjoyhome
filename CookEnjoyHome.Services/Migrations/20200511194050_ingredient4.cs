﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CookEnjoyHome.Services.Migrations
{
    public partial class ingredient4 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Receipts_Users_AuthorID",
                table: "Receipts");

            migrationBuilder.DropIndex(
                name: "IX_Receipts_AuthorID",
                table: "Receipts");

            migrationBuilder.DropColumn(
                name: "AuthorID",
                table: "Receipts");

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Receipts",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Receipts_UserID",
                table: "Receipts",
                column: "UserID");

            migrationBuilder.AddForeignKey(
                name: "FK_Receipts_Users_UserID",
                table: "Receipts",
                column: "UserID",
                principalTable: "Users",
                principalColumn: "UserID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Receipts_Users_UserID",
                table: "Receipts");

            migrationBuilder.DropIndex(
                name: "IX_Receipts_UserID",
                table: "Receipts");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Receipts");

            migrationBuilder.AddColumn<int>(
                name: "AuthorID",
                table: "Receipts",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Receipts_AuthorID",
                table: "Receipts",
                column: "AuthorID");

            migrationBuilder.AddForeignKey(
                name: "FK_Receipts_Users_AuthorID",
                table: "Receipts",
                column: "AuthorID",
                principalTable: "Users",
                principalColumn: "UserID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
