﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CookEnjoyHome.Services.Migrations
{
    public partial class ingredient3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ReceiptIngredient_Ingredients_IngredientID",
                table: "ReceiptIngredient");

            migrationBuilder.DropForeignKey(
                name: "FK_ReceiptIngredient_Receipts_ReceiptID",
                table: "ReceiptIngredient");

            migrationBuilder.DropForeignKey(
                name: "FK_Receipts_Users_AuthorUserID",
                table: "Receipts");

            migrationBuilder.DropIndex(
                name: "IX_Receipts_AuthorUserID",
                table: "Receipts");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ReceiptIngredient",
                table: "ReceiptIngredient");

            migrationBuilder.DropColumn(
                name: "AuthorUserID",
                table: "Receipts");

            migrationBuilder.RenameTable(
                name: "ReceiptIngredient",
                newName: "ReceiptIngredients");

            migrationBuilder.RenameIndex(
                name: "IX_ReceiptIngredient_ReceiptID",
                table: "ReceiptIngredients",
                newName: "IX_ReceiptIngredients_ReceiptID");

            migrationBuilder.RenameIndex(
                name: "IX_ReceiptIngredient_IngredientID",
                table: "ReceiptIngredients",
                newName: "IX_ReceiptIngredients_IngredientID");

            migrationBuilder.AddColumn<int>(
                name: "AuthorID",
                table: "Receipts",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AlterColumn<int>(
                name: "IngredientID",
                table: "ReceiptIngredients",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_ReceiptIngredients",
                table: "ReceiptIngredients",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_Receipts_AuthorID",
                table: "Receipts",
                column: "AuthorID");

            migrationBuilder.AddForeignKey(
                name: "FK_ReceiptIngredients_Ingredients_IngredientID",
                table: "ReceiptIngredients",
                column: "IngredientID",
                principalTable: "Ingredients",
                principalColumn: "IngredientID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ReceiptIngredients_Receipts_ReceiptID",
                table: "ReceiptIngredients",
                column: "ReceiptID",
                principalTable: "Receipts",
                principalColumn: "ReceiptID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Receipts_Users_AuthorID",
                table: "Receipts",
                column: "AuthorID",
                principalTable: "Users",
                principalColumn: "UserID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ReceiptIngredients_Ingredients_IngredientID",
                table: "ReceiptIngredients");

            migrationBuilder.DropForeignKey(
                name: "FK_ReceiptIngredients_Receipts_ReceiptID",
                table: "ReceiptIngredients");

            migrationBuilder.DropForeignKey(
                name: "FK_Receipts_Users_AuthorID",
                table: "Receipts");

            migrationBuilder.DropIndex(
                name: "IX_Receipts_AuthorID",
                table: "Receipts");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ReceiptIngredients",
                table: "ReceiptIngredients");

            migrationBuilder.DropColumn(
                name: "AuthorID",
                table: "Receipts");

            migrationBuilder.RenameTable(
                name: "ReceiptIngredients",
                newName: "ReceiptIngredient");

            migrationBuilder.RenameIndex(
                name: "IX_ReceiptIngredients_ReceiptID",
                table: "ReceiptIngredient",
                newName: "IX_ReceiptIngredient_ReceiptID");

            migrationBuilder.RenameIndex(
                name: "IX_ReceiptIngredients_IngredientID",
                table: "ReceiptIngredient",
                newName: "IX_ReceiptIngredient_IngredientID");

            migrationBuilder.AddColumn<int>(
                name: "AuthorUserID",
                table: "Receipts",
                type: "int",
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "IngredientID",
                table: "ReceiptIngredient",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddPrimaryKey(
                name: "PK_ReceiptIngredient",
                table: "ReceiptIngredient",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_Receipts_AuthorUserID",
                table: "Receipts",
                column: "AuthorUserID");

            migrationBuilder.AddForeignKey(
                name: "FK_ReceiptIngredient_Ingredients_IngredientID",
                table: "ReceiptIngredient",
                column: "IngredientID",
                principalTable: "Ingredients",
                principalColumn: "IngredientID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ReceiptIngredient_Receipts_ReceiptID",
                table: "ReceiptIngredient",
                column: "ReceiptID",
                principalTable: "Receipts",
                principalColumn: "ReceiptID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Receipts_Users_AuthorUserID",
                table: "Receipts",
                column: "AuthorUserID",
                principalTable: "Users",
                principalColumn: "UserID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
