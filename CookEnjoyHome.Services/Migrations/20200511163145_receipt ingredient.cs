﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CookEnjoyHome.Services.Migrations
{
    public partial class receiptingredient : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ReceiptIngredient",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Quantity = table.Column<int>(nullable: false),
                    IngredientID = table.Column<int>(nullable: false),
                    ReceiptID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ReceiptIngredient", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ReceiptIngredient_Receipts_ReceiptID",
                        column: x => x.ReceiptID,
                        principalTable: "Receipts",
                        principalColumn: "ReceiptID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ReceiptIngredient_ReceiptID",
                table: "ReceiptIngredient",
                column: "ReceiptID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ReceiptIngredient");
        }
    }
}
