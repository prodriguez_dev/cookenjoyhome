﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CookEnjoyHome.Services.Migrations
{
    public partial class ingredient2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "IngredientID",
                table: "ReceiptIngredient",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.CreateIndex(
                name: "IX_ReceiptIngredient_IngredientID",
                table: "ReceiptIngredient",
                column: "IngredientID");

            migrationBuilder.AddForeignKey(
                name: "FK_ReceiptIngredient_Ingredients_IngredientID",
                table: "ReceiptIngredient",
                column: "IngredientID",
                principalTable: "Ingredients",
                principalColumn: "IngredientID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ReceiptIngredient_Ingredients_IngredientID",
                table: "ReceiptIngredient");

            migrationBuilder.DropIndex(
                name: "IX_ReceiptIngredient_IngredientID",
                table: "ReceiptIngredient");

            migrationBuilder.AlterColumn<int>(
                name: "IngredientID",
                table: "ReceiptIngredient",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);
        }
    }
}
