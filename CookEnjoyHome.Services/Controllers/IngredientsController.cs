﻿using CookEnjoyHome.Services.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CookEnjoyHome.Services.Controllers
{
    [Route("api/[controller]")]
    public class IngredientsController : ControllerBase
    {
        private readonly BaseContext _context;

        public IngredientsController(BaseContext context)
        {
            _context = context;

        }

        // api/IngredientItems
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Ingredient>>> GetIngredientItems() => await _context.Ingredients.ToListAsync();

        // api/Ingredients/{id}
        [HttpGet("{id}")]
        public async Task<ActionResult<Ingredient>> GetIngredient(int id)
        {
            var ingredient = await _context.Ingredients.FindAsync(id);

            if (ingredient == null) return NotFound();

            return ingredient;
        }

        // api/Ingredients/{id}
        [HttpPut("{id}")]
        public async Task<IActionResult> PutIngredient(int id, Ingredient ingredient)
        {
            if (id != ingredient?.IngredientID) return BadRequest();

            _context.Update(ingredient);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        // api/Ingredients/
        [HttpPost]
        public async Task<ActionResult<Ingredient>> PostIngredient(Ingredient ingredient)
        {
            _context.Add(ingredient);
            await _context.SaveChangesAsync();

            return CreatedAtAction(nameof(GetIngredient), new { ingredient.IngredientID }, ingredient);
        }

        // api/Ingredients/{id}
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteIngredient(int id)
        {
            var ingredient = await _context.FindAsync<Ingredient>(id);

            if (ingredient == null) return NotFound();

            _context.Remove(ingredient);
            await _context.SaveChangesAsync();

            return NoContent();
        }
    }
}
