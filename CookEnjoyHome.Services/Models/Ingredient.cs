﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace CookEnjoyHome.Services.Models
{
    //[DataContract]
    public class Ingredient
    {
        //[DataMember]
        public int IngredientID { get; set; }
        public string Name { get; set; }
        public string Unit { get; set; }
    }
}
