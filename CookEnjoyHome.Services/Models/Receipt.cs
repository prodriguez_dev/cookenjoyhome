﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;

namespace CookEnjoyHome.Services.Models
{
    public class Receipt
    {
        public int ReceiptID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public ICollection<ReceiptIngredient> Ingredients { get; set; }
        public int UserID { get; set; }
        public User Author { get; set; }
    }
}
