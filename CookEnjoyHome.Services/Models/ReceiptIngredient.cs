﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CookEnjoyHome.Services.Models
{
    public class ReceiptIngredient
    {
        public int Id { get; set; }
        public int Quantity { get; set; }
        public int IngredientID { get; set; }
        public Ingredient Ingredient { get; set; }
        public int ReceiptID { get; set; }
    }
}
