﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CookEnjoyHome.Services.Models
{
    public class BaseContext: DbContext
    {
        public BaseContext() : base() { }
        public BaseContext(DbContextOptions<BaseContext> options) : base(options) { }

        public virtual DbSet<Receipt> Receipts { get; set; }
        public virtual DbSet<Ingredient> Ingredients { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<ReceiptIngredient> ReceiptIngredients { get; set; }
    }
}
