﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CookEnjoyHome.Services.Models;
using CookEnjoyHome.Web.Utility;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace CookEnjoyHome.Web.Controllers
{
    public class ReceiptsController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IOptions<AppSettings> _appSettings;
        private readonly WebAPIClient _apiClient;

        public ReceiptsController(IOptions<AppSettings> options)
        {
            _appSettings = options;
            _apiClient = new WebAPIClient(new Uri(_appSettings.Value.WebApiBaseUrl));
        }
        // GET: Receipts
        public async Task<IActionResult> IndexAsync()
        {
            var uri = _apiClient.CreateRequestUri("Receipts");           
            var data = await _apiClient.GetTAsync<IEnumerable<Receipt>>(uri);
            return View(data);
        }

        // GET: Receipts/Details/5
        public async Task<IActionResult> Details(int id)
        {
            var uri = _apiClient.CreateRequestUri("Receipts/" + id.ToString());
            var data = await _apiClient.GetTAsync<Receipt>(uri);
            return View(data);
        }

        // GET: Receipts/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Receipts/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Receipt receipt)
        {
            try
            {
                // TODO: Add insert logic here
                var uri = _apiClient.CreateRequestUri("Receipts");
                var data = await _apiClient.PostAsync<Receipt>(uri, receipt);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Receipts/Edit/5
        public async Task<IActionResult> Edit(int id)
        {
            var uri = _apiClient.CreateRequestUri("Receipts/" + id.ToString());
            var data = await _apiClient.GetTAsync<Receipt>(uri);
            return View(data);
        }

        // POST: Receipts/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, Receipt receipt)
        {
            try
            {
                // TODO: Add update logic here
                var uri = _apiClient.CreateRequestUri("Receipts/" + id.ToString());
                var data = await _apiClient.PutAsync(uri, receipt);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Receipts/Delete/5
        [HttpGet]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                // TODO: Add delete logic here
                var uri = _apiClient.CreateRequestUri($"Receipts/{id}");
                var data = await _apiClient.DeleteAsync<Receipt>(uri);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}