﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using CookEnjoyHome.Web.Models;
using Microsoft.Extensions.Options;
using CookEnjoyHome.Web.Utility;

namespace CookEnjoyHome.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IOptions<AppSettings> _appSettings;
        private readonly WebAPIClient _apiClient;

        public HomeController(ILogger<HomeController> logger, IOptions<AppSettings> options)
        {
            _logger = logger;            
            _appSettings = options;
            _apiClient = new WebAPIClient(new Uri(_appSettings.Value.WebApiBaseUrl));
        }

        public async Task<IActionResult> IndexAsync()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
