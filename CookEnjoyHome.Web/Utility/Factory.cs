﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CookEnjoyHome.Web.Utility
{
    // https://www.c-sharpcorner.com/article/consuming-web-apis-in-asp-net-core-mvc-application/
    public static class ApiClientFactory
    {
        private static Uri ApiUri;

        private static Lazy<WebAPIClient> restClient = new Lazy<WebAPIClient>(
            () => new WebAPIClient(ApiUri),
            System.Threading.LazyThreadSafetyMode.ExecutionAndPublication);

        static ApiClientFactory()
        {
            //ApiUri = new Uri(AppSettings.WebApiBaseUrl);
        }

        public static WebAPIClient Instance
        {
            get
            {
                return restClient.Value;
            }
        }
    }
}
