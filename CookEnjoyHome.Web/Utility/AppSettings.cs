﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CookEnjoyHome.Web.Utility
{
    public class AppSettings
    {
        public string WebApiBaseUrl { get; set; }
    }
}
